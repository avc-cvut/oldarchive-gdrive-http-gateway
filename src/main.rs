use oldarchive_gdrive_http_gateway::api::get_authetificator;
use oldarchive_gdrive_http_gateway::*;

fn main() {
    let port = std::env::var("GATEWAY_PORT").expect("env GATEWAY_PORT not set");
    std::env::var("ROOT_FOLDER_ID").expect("env ROOT_FOLDER_ID not set");

    let logger = setup_logger(slog::o!());
    let _scope_guard = slog_scope::set_global_logger(logger.clone());
    let _log_guard = slog_stdlog::init().unwrap();

    let mut sa_file =
        std::fs::File::open(std::env::var("SA_FILE").expect("Env SA_FILE not defined"))
            .expect("Could not open sa file");
    let mut sa_key_data = String::new();
    std::io::Read::read_to_string(&mut sa_file, &mut sa_key_data)
        .expect("Could not read from sa file");

    let authenticator = get_authetificator(&logger, &sa_key_data);

    let mut chain = iron::Chain::new(controller::controller);
    chain.link_before(persistent::Read::<AuthenticatorKey>::one(authenticator));

    slog::info!(&logger, "Starting GDrive HTTP Gateway on port {}", port);

    iron::Iron::new(chain)
        .http(format!("0.0.0.0:{}", port))
        .unwrap();
}
