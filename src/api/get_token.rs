use std::io;

use slog::Logger;
use slog_unwrap::ResultExt;
use yup_oauth2::authenticator::{Authenticator, DefaultAuthenticator};
use yup_oauth2::{self as oauth2, AccessToken};

pub fn get_authetificator(logger: &Logger, sa_key_data: &str) -> DefaultAuthenticator {
    let mut runtime = tokio::runtime::Runtime::new().expect("Could not start Tokio runtime");

    let sa_key: oauth2::ServiceAccountKey = serde_json::from_str(&sa_key_data)
        .map_err(|e| {
            io::Error::new(
                io::ErrorKind::InvalidData,
                format!("Bad service account key: {}", e),
            )
        })
        .unwrap_or_log(&logger);

    let authenticator =
        runtime.block_on(oauth2::ServiceAccountAuthenticator::builder(sa_key).build());

    authenticator.unwrap_or_log(&logger)
}

pub fn get_token<C>(authenticator: &Authenticator<C>) -> Result<AccessToken, AppTokenError>
where
    C: hyper::client::connect::Connect + Clone + Send + Sync + 'static,
{
    let mut runtime = tokio::runtime::Runtime::new().expect("Could not start Tokio runtime");

    runtime
        .block_on(authenticator.token(&["https://www.googleapis.com/auth/drive"]))
        .map_err(AppTokenError)
}

#[derive(Debug)]
pub struct AppTokenError(oauth2::Error);
