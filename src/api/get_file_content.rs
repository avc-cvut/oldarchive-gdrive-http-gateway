use crate::AppError;
use slog::Logger;

pub fn get_file_content(
    logger: &Logger,
    token: &str,
    file_id: &str,
) -> Result<reqwest::blocking::Response, AppError> {
    let client = reqwest::blocking::ClientBuilder::new()
        .gzip(true)
        .build()
        .unwrap();

    let req = client
        .get(format!("https://www.googleapis.com/drive/v2/files/{}", file_id).as_str())
        .header(reqwest::header::AUTHORIZATION, format!("Bearer {}", token))
        .query(&[("alt", "media")]);

    slog::debug!(logger, "Sending request to get file"; "request" => ?req);

    let response = req.send();

    slog::debug!(logger, "Got response from getting file"; "response" => ?response);

    let response = response.map_err(AppError::UnknownError)?;

    match response.status().as_u16() {
        200 => Ok(response),
        429 => Err(AppError::OverLoad),
        403 => Err(AppError::OverLoad),
        _ => Err(AppError::UnknownResponse(Box::new(response))),
    }
}
