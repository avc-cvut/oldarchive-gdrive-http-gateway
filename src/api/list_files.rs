use std::time::Instant;

use reqwest::header::HeaderMap;
use serde::Deserialize;
use slog::Logger;

use crate::AppError;

pub fn list_files_by_name(
    logger: &Logger,
    token: &str,
    parent_id: &str,
    name: &str,
    request_start_time: &Instant,
) -> Result<FilesListResponse, AppError> {
    let query = format!("name = '{}' and '{}' in parents", name, parent_id);

    list_files_with_retry(logger, token, &query, request_start_time)
}

pub fn list_files_in_folder(
    logger: &Logger,
    token: &str,
    parent_id: &str,
    request_start_time: &Instant,
) -> Result<FilesListResponse, AppError> {
    let query = format!("'{}' in parents", parent_id);

    list_files_with_retry(logger, token, &query, request_start_time)
}

fn list_files_with_retry(
    logger: &Logger,
    token: &str,
    query: &str,
    request_start_time: &Instant,
) -> Result<FilesListResponse, AppError> {
    use std::{thread, time};
    let mut pause_time = time::Duration::from_millis(200);

    loop {
        let result = list_files(logger, token, query);

        match result {
            Err(AppError::OverLoad) => (),
            _ => return result,
        }

        if request_start_time.elapsed() > crate::MAX_REQUEST_TIME {
            slog::debug!(logger, "Max backoff time elapsed"; "elapsedTime" => ?request_start_time.elapsed());
            return result;
        }

        slog::debug!(logger, "Overload, backoff retry"; "pauseTime" => ?pause_time);
        thread::sleep(pause_time);
        pause_time = pause_time.mul_f32(1.5);
    }
}

fn list_files(logger: &Logger, token: &str, query: &str) -> Result<FilesListResponse, AppError> {
    let client = reqwest::blocking::ClientBuilder::new()
        .gzip(true)
        .build()
        .unwrap();

    let req = client
        .get("https://www.googleapis.com/drive/v3/files")
        .header(reqwest::header::AUTHORIZATION, format!("Bearer {}", token))
        .query(&[("q", query)])
        .query(&[("fields", "files(kind,id,name,mimeType,parents) ")]);

    slog::debug!(logger, "Sending request to list files"; "request" => ?req);

    let response = req.send();

    slog::debug!(logger, "Got response from listing files"; "response" => ?response);

    let response = response.map_err(AppError::UnknownError)?;

    match response.status().as_u16() {
        200 => {
            let headers = response.headers().to_owned();
            let body = response
                .json::<FileListResponseBody>()
                .map_err(AppError::UnknownError)?;

            Ok(FilesListResponse { headers, body })
        }
        429 => Err(AppError::OverLoad),
        403 => Err(AppError::OverLoad),
        _ => Err(AppError::UnknownResponse(Box::new(response))),
    }
}

#[derive(Debug)]
pub struct FilesListResponse {
    pub headers: HeaderMap,
    pub body: FileListResponseBody,
}

#[derive(Debug, Deserialize)]
pub struct FileListResponseBody {
    pub files: Vec<FileListResponseEntry>,
}

#[derive(Debug, Deserialize)]
pub struct FileListResponseEntry {
    pub kind: String,
    pub id: String,
    pub name: String,

    #[serde(rename = "mimeType")]
    pub mime_type: String,
    pub parents: Vec<String>,
}
