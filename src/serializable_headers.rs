use std::collections::HashMap;

#[derive(Clone, serde::Serialize)]
pub struct SerializableHeaders {
    headers: HashMap<String, String>,
}

impl SerializableHeaders {
    pub fn new(headers: &iron::Headers) -> SerializableHeaders {
        let mut result: HashMap<String, String> = HashMap::new();
        for header in headers.iter() {
            result.insert(header.name().to_string(), header.value_string());
        }

        SerializableHeaders { headers: result }
    }
}

impl slog::SerdeValue for SerializableHeaders {
    fn as_serde(&self) -> &dyn erased_serde::Serialize {
        &self.headers
    }

    fn to_sendable(&self) -> Box<dyn slog::SerdeValue + Send> {
        Box::new(self.clone())
    }
}

impl slog::Value for SerializableHeaders {
    fn serialize(
        &self,
        _record: &slog::Record,
        key: &'static str,
        serializer: &mut dyn slog::Serializer,
    ) -> slog::Result {
        serializer.emit_serde(key, self)
    }
}
