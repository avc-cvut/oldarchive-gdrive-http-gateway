use std::time::Instant;

use http::header::AsHeaderName;
use iron::{self, mime, prelude::*, response::BodyReader};

use crate::api::get_token::get_token;
use crate::*;
use slog_unwrap::ResultExt;

pub fn controller(request: &mut Request) -> IronResult<Response> {
    let started_at = Instant::now();

    let logger = setup_logger(
        slog::o!("requestId" => uuid::Uuid::new_v4().to_string(), "path" => request.url.to_string()),
    );

    slog::info!(logger, "Got new request"; "url" => request.url.to_string(), "headers" => serializable_headers::SerializableHeaders::new(&request.headers));

    let root_folder = std::env::var("ROOT_FOLDER_ID").expect("env ROOT_FOLDER_ID not set");

    let token = get_token(
        &request
            .get::<persistent::Read<AuthenticatorKey>>()
            .unwrap_or_log(&logger),
    )
    .unwrap_or_log(&logger);

    let result = gdrive_file_content(&logger, token.as_ref(), &root_folder, &request.url.path())
        .map_err(|e| map_error(&logger, &e))?;

    let status = iron::status::Status::from_u16(result.status().as_u16());
    let mut headers = iron::headers::Headers::new();

    let content_type: Option<mime::Mime> = get_header_value(&result, reqwest::header::CONTENT_TYPE);
    if let Some(content_type) = content_type {
        match content_type {
            mime::Mime(mime::TopLevel::Text, mime::SubLevel::Html, _) => {
                slog::debug!(logger, "Matched HTML mimetype");
            }
            _ => {
                slog::debug!(logger, "Matched non-HTML mimetype");
                headers.set(iron::headers::ContentDisposition {
                    disposition: iron::headers::DispositionType::Attachment,
                    parameters: Vec::default(),
                });
            }
        }
        headers.set(iron::headers::ContentType(content_type));
    }

    if let Some(content_length) = get_header_value(&result, reqwest::header::CONTENT_LENGTH) {
        headers.set(iron::headers::ContentLength(content_length))
    }

    headers.set(iron::headers::CacheControl(vec![
        iron::headers::CacheDirective::Public,
        iron::headers::CacheDirective::Extension("immutable".to_string(), None),
        iron::headers::CacheDirective::MaxAge(60 * 24 * 356),
    ]));

    //TODO: Implement
    // let accept_ranges = iron::headers::AcceptRanges(vec![iron::headers::RangeUnit::Bytes]);
    // headers.set(accept_ranges);

    let content = BodyReader { 0: result };
    let mut response = Response::with((status, content));
    response.headers = headers;

    slog::info!(logger, "Successful response"; "duration_sec" => started_at.elapsed().as_secs_f64());

    Ok(response)
}

fn get_header_value<H: AsHeaderName, R: FromStr>(
    response: &reqwest::blocking::Response,
    header: H,
) -> Option<R> {
    response
        .headers()
        .get(header)?
        .to_str()
        .ok()?
        .parse::<R>()
        .ok()
}

fn map_error(logger: &Logger, app_error: &AppError) -> IronError {
    match app_error {
        AppError::OverLoad => {
            slog::error!(logger, "Service overload");
            IronError::new(
                AppError::OverLoad,
                (
                    iron::status::ServiceUnavailable,
                    "Error 503: Service temporarily unavailable", //TODO: Nice html page
                ),
            )
        }
        AppError::NotFound => {
            slog::warn!(logger, "File not found");
            IronError::new(
                AppError::NotFound,
                (
                    iron::status::NotFound,
                    "Error 404: Not Found", //TODO: Nice html page
                ),
            )
        }
        _ => {
            slog::error!(logger, "Unknown error"; "error" => ? app_error);
            IronError::new(
                AppError::Unknown,
                (
                    iron::status::InternalServerError,
                    "Error 500: Server error", //TODO: Nice html page
                ),
            )
        }
    }
}
