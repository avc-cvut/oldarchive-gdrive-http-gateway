use core::fmt;
use std::str::FromStr;

use iron::Error;
use serde::export::Formatter;
use slog::Logger;
use yup_oauth2::authenticator::DefaultAuthenticator;

use crate::AppError::NotFound;
use std::time;

pub mod api;
pub mod controller;
pub mod serializable_headers;

// When to give up on retries
static MAX_REQUEST_TIME: time::Duration = time::Duration::from_secs(20);

#[derive(Copy, Clone)]
pub struct AuthenticatorKey;

impl iron::typemap::Key for AuthenticatorKey {
    type Value = DefaultAuthenticator;
}

#[derive(Debug)]
pub enum AppError {
    NotFound,
    OverLoad,
    UnknownError(reqwest::Error),
    UnknownResponse(Box<reqwest::blocking::Response>),
    Unknown,
}

impl fmt::Display for AppError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(self, f)
    }
}

impl std::error::Error for AppError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        None
    }
}

pub fn gdrive_file_content(
    logger: &Logger,
    token: &str,
    root_folder: &str,
    path: &[&str],
) -> Result<reqwest::blocking::Response, AppError> {
    let request_start_time = time::Instant::now();

    let mut current_folder = root_folder.to_string();

    let mut traverse = Vec::<api::list_files::FilesListResponse>::default();

    // Only for logger
    let path_string = path.join("/");

    let path = if path.last().ok_or(NotFound)?.is_empty() {
        path.split_last().ok_or(NotFound)?.1
    } else {
        path
    };

    for path_segment in path {
        let list_result = api::list_files_by_name(
            &logger,
            token,
            &current_folder,
            path_segment,
            &request_start_time,
        )?;

        let files = &list_result.body.files;

        if files.len() != 1 {
            if files.len() > 1 {
                slog::error!(
                logger,
                "Multiple files with same name in current folder";
                "path" => path_string,
                "current_folder" => path_segment
                );
            }

            return Result::Err(NotFound);
        }

        current_folder = files.first().ok_or(NotFound)?.id.clone();

        traverse.push(list_result);
    }

    let file = &traverse
        .last()
        .ok_or(NotFound)?
        .body
        .files
        .first()
        .ok_or(NotFound)?;

    let file_listing; // Just a "hack" to keep temporary value owned somewhere
    let file = if file.mime_type == "application/vnd.google-apps.folder" {
        file_listing = api::list_files_in_folder(&logger, token, &file.id, &request_start_time)?;
        file_listing
            .body
            .files
            .iter()
            .find(|i| ["index.html".to_string(), "index.htm".to_string()].contains(&i.name)) // TODO: Some coercing problem, to_string probably should not be required. There could also be better solution
            .ok_or(AppError::NotFound)?
    } else {
        file
    };

    api::get_file_content(&logger, token, &file.id)
}

pub fn setup_logger<T>(custom_keys: slog::OwnedKV<T>) -> Logger
where
    T: slog::SendSyncRefUnwindSafeKV + 'static,
{
    use slog::{Drain, LevelFilter};

    let drain = slog_json::Json::new(std::io::stderr())
        .add_default_keys()
        .add_key_value(custom_keys)
        .build()
        .fuse();
    let drain = slog_async::Async::new(drain).build().fuse();
    let drain = LevelFilter::new(
        drain,
        slog::Level::from_str(
            std::env::var("RUST_LOG")
                .unwrap_or_else(|_| String::from("INFO"))
                .as_str(),
        )
        .unwrap(),
    )
    .fuse();
    slog::Logger::root(drain, slog::o!())
}
